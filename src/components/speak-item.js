import Timestamp from './timestamp';

class SpeakItem {
  /**
   * @param {firebase.firestore.QueryDocumentSnapshot} ref
   * @param {string} yourName
   */
  constructor(ref, yourName) {
    this.ref = ref;
    this.data = ref.data();
    this.yourName = yourName;
  }

  /**
   * @returns {string}
   */
  get id() {
    return this.ref.id;
  }

  /**
   * @returns {string}
   */
  get name() {
    return this.data.name;
  }

  /**
   * @returns {string}
   */
  get text() {
    return this.data.text;
  }

  /**
   * @returns {Timestamp}
   */
  get timestamp() {
    return new Timestamp(this.data.timestamp);
  }

  /**
   * @returns {boolean}
   */
  get yourSpeak() {
    return this.name === this.yourName;
  }
}

export default SpeakItem;
