import moment from 'moment';

class Timestamp {
  /**
   * @param {firebase.firestore.Timestamp} value
   */
  constructor(value) {
    this.value = value;
  }

  toString() {
    return this.value ? moment(this.value.toDate()).fromNow() : '';
  }
}

export default Timestamp;
