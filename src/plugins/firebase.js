import * as firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCh73lesx1UCSWeMIDvfUG7-0H1ucv4yyI',
  authDomain: 'chat-e2745.firebaseapp.com',
  databaseURL: 'https://chat-e2745.firebaseio.com',
  projectId: 'chat-e2745',
  storageBucket: 'chat-e2745.appspot.com',
  messagingSenderId: '1029622094557',
  appId: '1:1029622094557:web:f987a90d6bafc53f551934',
};

firebase.initializeApp(firebaseConfig);
