# チャット
[かごもく #26 Flutter](https://kagoben.connpass.com/event/153838/) で作成する[チャットアプリ](https://masakura.gitlab.io/chat/)の Vue.js のリファレンス。

これをまねて、[Flutter](https://flutter.dev/) で作ってみましょう!


## チャットをする
https://masakura.gitlab.io/chat/ にアクセスしてください。


## Firebase の簡単な使い方
Firebase の初期化。

```javasxcript
import * as firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCh73lesx1UCSWeMIDvfUG7-0H1ucv4yyI',
  authDomain: 'chat-e2745.firebaseapp.com',
  databaseURL: 'https://chat-e2745.firebaseio.com',
  projectId: 'chat-e2745',
  storageBucket: 'chat-e2745.appspot.com',
  messagingSenderId: '1029622094557',
  appId: '1:1029622094557:web:f987a90d6bafc53f551934',
};

firebase.initializeApp(firebaseConfig);
```

読んだり書いたり。

```javascript
import * as firebase from 'firebase/app';

const db = firebase.firestore();

// 読み込む
const speaks = db.collection('speaks').orderBy('timestamp');
speaks.onSnapshot((snapshot) => {
  // 更新がされるたびにここが呼び出される
  const items = snapshot.docs.map(item => ({
    id: item.id,
    data: item.data(),
  }));
  // [
  //    { id: ..., data: { name: ..., text: ..., timestamp: ... }},
  //    ...
  // ]
  // timestamp は Firestore の Timestamp 型なので、toDate() を呼び出してくださいa
});

// 追加する
db.collection('speaks').add({
  name: '太郎',
  text: 'ほげー',
  timestamp: firebase.firestore.FieldValue.serverTimestamp(),
});
```


## Firebase Console を使いたい人
デバッグのために [Firebase Console](https://console.firebase.google.com/project/chat-e2745/database/firestore/data~2F) にアクセスしたい人はまさくらまで。管理者に加えます!


## 開発 (Vue.js 版の)
事前に [Node.js](https://nodejs.org/ja/) の LTS をインストールしてください。

ソースコードをチェックアウトして初期化します。

```
git clone https://gitlab.com/masakura/chat.git
cd chat
npm install
```

実行します。ブラウザーを起動して、http://localhost:8080 にアクセスしてください。

```
npm run serve
```